﻿﻿using UnityEngine;

/// <summary>
/// Removal Pointer.
/// Calculates world position of user input and passes it to the mesh.
/// </summary>
public class RemovalPointer : MonoBehaviour
{
    // Reference to the input layer to attach actions.
    public InputLayer inputLayer;
    // Reference to the mesh controller.
    public MeshController mesh;

    // Reference to the main camera.
    public Camera main;

    // Removal radius.
    public float hitRadius = 2;

    /// <summary>
    /// Unity method called when component is enabled.
    /// </summary>
    private void OnEnable()
    {
        inputLayer.PointerDown += OnPointerDown;
        inputLayer.DragBegin += OnDragBegin;
        inputLayer.Drag += OnDrag;
        inputLayer.DragEnd += OnDragEnd;
    }

    /// <summary>
    /// Unity method called when component is disabled.
    /// </summary>
    private void OnDisable()
    {
        inputLayer.PointerDown -= OnPointerDown;
        inputLayer.DragBegin -= OnDragBegin;
        inputLayer.Drag -= OnDrag;
        inputLayer.DragEnd -= OnDragEnd;
    }

    /// <summary>
    /// Action on begin drag.
    /// </summary>
    /// <param name="screenPosition">Screen position.</param>
    private void OnDragBegin(Vector2 screenPosition)
    {
        RemovePartsOfMeshAt(screenPosition);
    }

    /// <summary>
    /// Action on drag.
    /// </summary>
    /// <param name="screenPosition">Screen position.</param>
    private void OnDrag(Vector2 screenPosition)
    {
        RemovePartsOfMeshAt(screenPosition);
    }

    /// <summary>
    /// Action on end drag.
    /// </summary>
    /// <param name="screenPosition">Screen position.</param>
    private void OnDragEnd(Vector2 screenPosition)
    {
        RemovePartsOfMeshAt(screenPosition);
    }

    /// <summary>
    /// Action on pointer down.
    /// </summary>
    /// <param name="screenPosition">Screen position.</param>
    private void OnPointerDown(Vector2 screenPosition)
    {
        RemovePartsOfMeshAt(screenPosition);
    }

    /// <summary>
    /// Removes parts of the mesh at Screen Position.
    /// </summary>
    /// <param name="screenPosition">Screen position.</param>
    private void RemovePartsOfMeshAt(Vector2 screenPosition)
    {
        // Calculating world position.
        var worldPoint = main.ScreenToWorldPoint(screenPosition);
        var hitPoint = new Vector2(worldPoint.x, worldPoint.y);

        // Passing data to the mesh to remove its parts.
        mesh.RemoveMeshAt(hitPoint, hitRadius);
    }
}

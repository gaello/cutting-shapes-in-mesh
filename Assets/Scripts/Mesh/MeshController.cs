﻿using UnityEngine;

/// <summary>
/// This class controls the mesh and removal of its parts.
/// </summary>
[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class MeshController : MonoBehaviour
{
    // Width of the mesh.
    public int width = 10;
    // Height of the mesh.
    public int height = 20;

    // Size of each quad in the mesh.
    public float tileSize = 0.8f;

    // 2-dimensional array that the mesh is based on.
    private bool[,] wallGrid;

    // Reference to MeshFilter.
    private MeshFilter meshFilter;

    // Position where grid begins.
    // Created for convenience.
    private Vector3 gridBegin;

    /// <summary>
    /// Unity method called on object creation.
    /// </summary>
    private void Awake()
    {
        meshFilter = GetComponent<MeshFilter>();

        // Calculating where grid should begin.
        gridBegin = new Vector3((-width / 2f + .5f) * tileSize, (-height / 2f + .5f) * tileSize, 0);

        // Creting 2-dimensional array for the mesh.
        wallGrid = new bool[width, height];
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                wallGrid[x, y] = true;
            }
        }
    }

    /// <summary>
    /// Unity method called on the first frame.
    /// </summary>
    void Start()
    {
        GenerateMesh();
    }

    /// <summary>
    /// Method responsible for generating mesh based on 2-dimensional array.
    /// </summary>
    private void GenerateMesh()
    {
        // Getting mesh instance.
        Mesh mesh = meshFilter.mesh;

        if (mesh == null)
        {
            mesh = new Mesh();
        }
        mesh.Clear();

        // Counting how many quads needs to be created.
        int w = 0;
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                if (wallGrid[x, y])
                {
                    w++;
                }
            }
        }

        // Preparing variables for creating a mesh.
        var vertices = new Vector3[w * 4];
        var triangles = new int[w * 6];

        int vertIndex = 0;
        int triIndex = 0;

        // Caching few calculations for the quad.
        var bottomLeft = new Vector3(-tileSize / 2, -tileSize / 2, 0);
        var topLeft = new Vector3(-tileSize / 2, tileSize / 2, 0);
        var topRight = new Vector3(tileSize / 2, tileSize / 2, 0);
        var bottomRight = new Vector3(tileSize / 2, -tileSize / 2, 0);

        // Looping over 2-dimensional array to generate quads.
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                if (wallGrid[x, y])
                {
                    // Calculating center of the quad.
                    var center = gridBegin + new Vector3(x, y, 0) * tileSize;

                    // Assigning vertices
                    vertices[vertIndex + 0] = (center + bottomLeft);
                    vertices[vertIndex + 1] = (center + topLeft);
                    vertices[vertIndex + 2] = (center + topRight);
                    vertices[vertIndex + 3] = (center + bottomRight);

                    // Assigning triangles
                    triangles[triIndex + 0] = (vertIndex + 0);
                    triangles[triIndex + 1] = (vertIndex + 1);
                    triangles[triIndex + 2] = (vertIndex + 2);

                    triangles[triIndex + 3] = (vertIndex + 0);
                    triangles[triIndex + 4] = (vertIndex + 2);
                    triangles[triIndex + 5] = (vertIndex + 3);

                    // Increasing indexes for vertices and triangles.
                    vertIndex += 4;
                    triIndex += 6;
                }
            }
        }

        // Assigning arrays to the mesh.
        mesh.vertices = vertices;
        mesh.triangles = triangles;

        // Updating mesh.
        meshFilter.mesh = mesh;

        // Cleaning arrays.
        vertices = null;
        triangles = null;
    }

    /// <summary>
    /// Removes parts of the mesh at provided positions and range.
    /// </summary>
    /// <param name="position">Position in world.</param>
    /// <param name="range">Range of removing.</param>
    public void RemoveMeshAt(Vector2 position, float range)
    {
        // Converting world position to the indexes in 2-dimensional array.
        var positionIndex = ((position + Vector2.one * tileSize * .5f) - new Vector2(gridBegin.x, gridBegin.y)) / tileSize;
        // Converting range into index range.
        var indexRange = range / tileSize;

        // Calculating square in which elements are being removed
        // Width
        int startX = Mathf.CeilToInt(Mathf.Max(0, positionIndex.x - indexRange));
        int endX = Mathf.FloorToInt(Mathf.Min(width, positionIndex.x + indexRange));

        // Height
        int startY = Mathf.CeilToInt(Mathf.Max(0, positionIndex.y - indexRange));
        int endY = Mathf.FloorToInt(Mathf.Min(height, positionIndex.y + indexRange));

        // Flag if we need to update a mesh.
        bool needUpdateMesh = false;

        // Looping through array to remove elements in range.
        for (int x = startX; x < endX; x++)
        {
            for (int y = startY; y < endY; y++)
            {
                // Check if element is in range of removal
                if (Vector2.Distance(positionIndex, new Vector2(x, y)) > indexRange)
                {
                    continue;
                }

                // If part of mesh exists and it is being removed, we have to update a mesh.
                if (wallGrid[x, y])
                {
                    needUpdateMesh = true;
                }

                wallGrid[x, y] = false;
            }
        }

        // If update in mesh is needed, run generate mesh again.
        if (needUpdateMesh)
        {
            GenerateMesh();
        }
    }
}
